class AddCaseTypeToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :case_type, :integer, default: 1
  end
end
