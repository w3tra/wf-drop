class AddCreditToLivedropGames < ActiveRecord::Migration[5.1]
  def change
    add_column :livedrop_games, :credits, :integer
  end
end
