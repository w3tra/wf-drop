class AddCaseOpenedToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :case_opened, :integer, default: 0
  end
end
