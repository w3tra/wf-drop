class AddCreditsHistoryToUsers < ActiveRecord::Migration[5.1]
  def change
    add_column :users, :credit_history, :integer, default: 0
  end
end
