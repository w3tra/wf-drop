class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.references :user, foreign_key: true
      t.integer :amount, default: 0
      t.boolean :finsihed, default: false

      t.timestamps
    end
  end
end
