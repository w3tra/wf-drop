class CreateLivedropGames < ActiveRecord::Migration[5.1]
  def change
    create_table :livedrop_games do |t|
      t.references :user, foreign_key: true
      t.string :weapon
      t.string :weapon_img
      t.string :string

      t.timestamps
    end
  end
end
