class PostsController < ApplicationController
  before_action :set_post, only: [:show, :edit, :update, :destroy]

  def index
    respond_to do |format|
      format.html
      format.js
    end
  end

  def new
    @post = Post.new
  end

  def questions
    
  end

  def switch_case
    if user_signed_in?
      current_user.switch_case(params[:case_type])
      render 'posts/switch_case'
      #redirect_back(fallback_location: root_path)
    else
      redirect_to user_vkontakte_omniauth_authorize_path
    end
  end

  def create
    @post = Post.new(post_params)
  end

  def top
    @top_users = User.order(credit_history: :desc).limit(7)
    @avatars = []
    @top_users.each_with_index do |user, index|
      first_uri = URI.parse "https://api.vk.com/method/users.get?user_id=#{user.uid}&fields=photo_100&v=5.52&access_token=61183cdd61183cdd61183cddfb6145300a6611861183cdd3841350b99693d0b1a2c277d"
      first_result = Net::HTTP.get(first_uri)
      first_parsed_result = JSON.parse(first_result)
      first_avatar = first_parsed_result['response'][0]['photo_100']
      @avatars << first_avatar
    end
    @avatars
  end


  private
    # Use callbacks to share common setup or constraints between actions.
    def set_post
      @post = Post.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def post_params
      params.require(:post).permit(:name, :description)
    end
end
