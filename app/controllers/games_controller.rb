class GamesController < ApplicationController
  def play
    @user = current_user
    if @user.case_type == 1
      if @user.balance >= 79
        @user.vip? ? @game = vip_game1 : @game = game1
        @random = @game[:item_number]
        @user.balance -= 79
      else
        redirect_to root_path, notice: 'no_money'
      end
    elsif @user.case_type == 2
      if @user.balance >= 159
        @user.vip? ? @game = vip_game2 : @game = game2
        @random = @game[:item_number]
        @user.balance -= 159
      else
        redirect_to root_path, notice: 'no_money'
      end
    else
      redirect_to root_path
    end
    @user.credits += @game[:item_price]
    @user.credit_history += @game[:item_price]
    @user.case_opened += 1
    @user.save
    @livedrop_game = @user.livedrop_games.create(credits: @game[:item_price], weapon: @game[:item_number])
    @livedrop_game.save
    render 'games/play', format: :js
  end

  def game1
    rand_number = rand(0..10_000)
    case rand_number
    when 0..2499      then { item_number: 1,  item_price: 30 }
    when 2500..5099   then { item_number: 2,  item_price: 60 }
    when 5417..7000   then { item_number: 2,  item_price: 60 }
    when 7050..10_000 then { item_number: 3,  item_price: 90 }
    when 5100..5250   then { item_number: 4,  item_price: 160 }
    when 5251..5300   then { item_number: 5,  item_price: 200 }
    when 5301..5400   then { item_number: 6,  item_price: 300 }
    when 7001..7049   then { item_number: 6,  item_price: 300 }
    when 5401..5410   then { item_number: 7,  item_price: 500 }
    when 5411..5412   then { item_number: 8,  item_price: 750 }
    when 5413..5414   then { item_number: 9,  item_price: 1000 }
    when 5415..5416   then { item_number: 10, item_price: 2000 }
    end
end

def game2
    rand_number = rand(0..10_000)
    case rand_number
    when 0..1000     then   { item_number: 17,  item_price: 40 }
    when 1001..5000  then   { item_number: 18,  item_price: 80 }
    when 5421..10000 then   { item_number: 19,  item_price: 160 }
    when 5001..5200  then   { item_number: 20,  item_price: 320 }
    when 5201..5300  then   { item_number: 21,  item_price: 400 }
    when 5301..5400  then   { item_number: 22,  item_price: 500 }
    when 5401..5410  then   { item_number: 23,  item_price: 750 }
    when 5411..5412  then   { item_number: 24,  item_price: 1000 }
    when 5413..5414  then   { item_number: 25,  item_price: 1500 }
    when 5415..5416  then   { item_number: 26,  item_price: 2500 }
    when 5416..5417  then   { item_number: 27,  item_price: 5000 }
    when 5418..5419  then   { item_number: 28,  item_price: 7500 }
    when 5419..5420  then   { item_number: 29,  item_price: 10000 }
    end
end

def vip_game1
    rand_number = rand(0..10_000)
    case rand_number
    when 0..1000      then { item_number: 1,  item_price: 30 }
    when 1001..4500   then { item_number: 2,  item_price: 60 }
    when 4501..6900   then { item_number: 3,  item_price: 90 }
    when 6901..8000   then { item_number: 4,  item_price: 160 }
    when 8001..8200   then { item_number: 5,  item_price: 200 }
    when 8201..8400   then { item_number: 6,  item_price: 300 }
    when 8401..8600   then { item_number: 7,  item_price: 500 }
    when 8601..8800   then { item_number: 8,  item_price: 750 }
    when 8801..9000   then { item_number: 9,  item_price: 1000 }
    when 9001..9200   then { item_number: 10, item_price: 2000 }
    when 9201..9400   then { item_number: 11, item_price: 3500 }
    when 9401..9600   then { item_number: 12, item_price: 5000 }
    when 9601..9700   then { item_number: 13, item_price: 7500 }
    when 9701..9800   then { item_number: 14, item_price: 10000 }
    when 9801..9900   then { item_number: 13, item_price: 15000 }
    when 9901..10000  then { item_number: 14, item_price: 25000 }
    end
end

def vip_game2
    rand_number = rand(0..10_000)
    case rand_number
    when 0..1000     then   { item_number: 17,  item_price: 40 }
    when 1001..4500  then   { item_number: 18,  item_price: 80 }
    when 4501..6900  then   { item_number: 19,  item_price: 160 }
    when 6901..7000  then   { item_number: 20,  item_price: 320 }
    when 7001..8200  then   { item_number: 21,  item_price: 400 }
    when 8201..8400  then   { item_number: 22,  item_price: 500 }
    when 8401..8600  then   { item_number: 23,  item_price: 750 }
    when 8601..8800  then   { item_number: 24,  item_price: 1000 }
    when 8801..9000  then   { item_number: 25,  item_price: 1500 }
    when 9001..9200  then   { item_number: 26,  item_price: 2500 }
    when 9201..9400  then   { item_number: 27,  item_price: 5000 }
    when 9401..9600  then   { item_number: 28,  item_price: 7500 }
    when 9601..9700  then   { item_number: 29,  item_price: 10000 }
    when 9701..9800  then   { item_number: 30,  item_price: 15000 }
    when 9801..9900  then   { item_number: 31,  item_price: 25000 }
    when 9901..10000 then   { item_number: 32,  item_price: 35000 }
    end
end

end

