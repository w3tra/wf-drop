class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  before_action :logout_if_inactive
  after_action :user_activity

  private

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:name])
    devise_parameter_sanitizer.permit(:account_update, keys: [:name, :telephone])
  end

  def logout_if_inactive
    sign_out :user unless user_signed_in? && current_user.active?
  end

  def user_activity
    current_user.try :touch
  end
end
