class OrdersController < ApplicationController
  protect_from_forgery except: [:result]
  before_action :authenticate_user!, except: [:result]

  def new
    @order = Order.new
  end

  def create
    @order = current_user.orders.create(order_params)
    if @order.save
      redirect_to confirm_order_path(@order)
    else
      redirect_to root_path
    end
  end

  def confirm
    @order = Order.find(params[:id])
    redirect_to root_path unless @order.user == current_user && !@order.finished?
    @merchant_id = '52232'
    @secret_word = '7p5tn03t'
    @order_id = @order.id.to_s
    @order_amount = @order.amount.to_s
    @sign = Digest::MD5.hexdigest(@merchant_id + ':' + @order_amount + ':' + @secret_word + ':' + @order_id)
  end

  def result
    @merchant_id = '52232'
    @secret_word2 = '0rxhrvk2'
    @params = request.params
    @amount = params['AMOUNT']
    @order_id = params['MERCHANT_ORDER_ID'].to_i
    @sign = params['SIGN']
    @order = Order.find(@order_id)
    @sample_sign = Digest::MD5.hexdigest("#{@merchant_id}:#{@order.amount}:#{@secret_word2}:#{@order.id}")
    if @sign == @sample_sign
      @order.complete!
      @user = @order.user
      @user.balance += @order.amount
      @user.save
    else
      redirect_to fail_order_path
    end
  end

  def fail
  end

  private

  def order_params
    params.require(:order).permit(:amount)
  end
end
