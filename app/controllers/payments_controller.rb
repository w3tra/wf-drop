class PaymentsController < ApplicationController
  def new
  	@payment = Payment.new
  end

  def create
    redirect_to root_path, notice: 'Минимальная сумма вывода 300 кредитов' if current_user.credits < 300
  	@payment = current_user.payments.create(payment_params)
  	if @payment.amount <= current_user.credits
  		@response = RestClient.post'https://money.yandex.ru/api/request-payment',{pattern_id: '81361', customerNumber: @payment.email, sum: (@payment.amount / 2).to_s},{Authorization: 'Bearer 410011314199927.E6DB787E2FDC83E92C0309AE9EFCFCDB29A48B89B11DE0A3E8B2CA28CE49F0B41F888A6D355073F392391A84004E6B371B62BD9356F1DB57DD36C19B49A5560ECEFA0678FA3670B58B2B91B20FDABFA3708BA458232ED1F1AD856BD257A0A5AC6AEF9A190B09E751006C3E3959C80C6B354708DC36A33F630FA193A2005607E9'}
    	@parsed_response = JSON.parse(@response)
    	@request_id = @parsed_response['request_id']
    	p @request_id
    	if @parsed_response['status'] == 'success'
    		@response = RestClient.post'https://money.yandex.ru/api/process-payment', {"request_id": @request_id}, {Authorization: 'Bearer 410011314199927.E6DB787E2FDC83E92C0309AE9EFCFCDB29A48B89B11DE0A3E8B2CA28CE49F0B41F888A6D355073F392391A84004E6B371B62BD9356F1DB57DD36C19B49A5560ECEFA0678FA3670B58B2B91B20FDABFA3708BA458232ED1F1AD856BD257A0A5AC6AEF9A190B09E751006C3E3959C80C6B354708DC36A33F630FA193A2005607E9'}
    		parsed_response2 = JSON.parse(@response)
    		if parsed_response2['status'] == 'success'
    			@payment.user.credits -= @payment.amount
    			@payment.user.save
    			redirect_to root_path, notice: 'ok'
    		else
    			redirect_to root_path, notice: 'bad' + parsed_response2['error']
    		end
    	end
    else
    	redirect_to root_path, notice: 'Недостаточно средств'
  	end

  end

  private

  def payment_params
  	params.require(:payment).permit(:amount, :email)
  end
end
