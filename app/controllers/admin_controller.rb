class AdminController < ApplicationController
	before_action :authenticate_user!
	before_action :check_user_is_admin
	layout 'admin'

	private

	def check_user_is_admin
		redirect_to root_path unless current_user.admin?
	end
end