ActiveAdmin.register User do
  actions :all, except: [:destroy]

  controller do
    def update
      if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        params[:user].delete("password")
        params[:user].delete("password_confirmation")
      end
      super
    end
  end

  config.clear_action_items!

  action_item('Изменить пользователя', only: :show) do
    link_to('Изменить Пользователя' , edit_admin_user_path(user))
  end

  permit_params :balance, :credits, :active, :vip

  index do
    column :id
    column :email
    column :name
    column :uid
    column :active
    column :vip
    column :credit_history
    column :balance
    actions
  end
  filter :email
  filter :name
  filter :uid

  form do |f|
    f.inputs do
      f.input :balance
      f.input :credits
      f.input :active
      f.input :vip
    end
    f.actions
  end

  show do
    attributes_table do
      row :name
      row :email
      row :balance
      row :credits
      row :credit_history
      row :active
      row :vip
    end
    active_admin_comments
  end
end
