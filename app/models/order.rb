#Order models
class Order < ApplicationRecord
  belongs_to :user
  validates :amount, numericality: { only_integer: true }

  def complete!
    update_attribute(:finished, true)
  end
end
