class User < ApplicationRecord
  has_many :orders
  has_many :payments
  has_many :livedrop_games
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :omniauthable, :omniauth_providers => [:vkontakte]

  def self.from_omniauth(auth)
    where(provider: auth["provider"], uid: auth["uid"]).first_or_create do |user|
      user.email = auth["info"]["email"]
      user.email = Digest::MD5.hexdigest(auth["info"]["name"]) + 'vk' + rand(111111..999999).to_s + '@wf-drop.ru' if user.email.nil?
      user.name = auth["info"]["name"]   # assuming the user model has a name
      # If you are using confirmable and the provider(s) you use validate emails,
      # uncomment the line below to skip the confirmation emails.
      # user.skip_confirmation!
    end
  end

  def change_status
    active? ? (update_attribute :active, false) : (update_attribute :active, true)
  end

  def online?
    updated_at > 10.minutes.ago
  end

  def change_admin
    admin? ? (update_attribute :admin, false) : (update_attribute :admin, true)
  end

  def switch_case(case_type)
    update_attribute(:case_type, case_type)
  end

end
