Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  get 'payments/new'

  get 'orders/new'

  get 'orders/create'

  get 'orders/confirm'

  get 'orders/result'

  get 'orders/fail'

  get 'games/play'

  root 'posts#index'
  resources :payments
  resources :orders do
    member do
      get :confirm
    end
    collection do
      get :fail
      post :result
    end
  end
  resources :posts do
    collection do
      get :questions
      get :top
      get :switch_case
    end
  end
  devise_for :users,
             controllers: { omniauth_callbacks: 'users/omniauth_callbacks' }
  devise_scope :user do
    delete 'sign_out', :to => 'devise/sessions#destroy', :as => :destroy_user_session
  end
  namespace :admin do
    root 'users#index'
    resources :users do
      member do
        post :change_status
        post :change_admin
        post :change_balance
      end
    end
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
