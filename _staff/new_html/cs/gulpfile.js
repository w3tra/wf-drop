// Инициализируем плагины
var lr = require('tiny-lr'), // Минивебсервер для livereload
    gulp = require('gulp'), // Сообственно Gulp JS
    jade = require('gulp-jade'), // Плагин для Jade
    stylus = require('gulp-stylus'), // Плагин для Stylus
    livereload = require('gulp-livereload'), // Livereload для Gulp
    myth = require('gulp-myth'), // Плагин для Myth - http://www.myth.io/
    csso = require('gulp-csso'), // Минификация CSS
    imagemin = require('gulp-image'), // Минификация изображений
    uglify = require('gulp-uglify'), // Минификация JS
    concat = require('gulp-concat'), // Склейка файлов
    serveStatic = require('serve-static'),
    connect = require('connect'), // Webserver
    server = lr();


// Собираем Stylus
gulp.task('css-plugin', function() {
    gulp.src(['./assets/css/**/*.css', '!./assets/css/main.css', '!./assets/css/media.css'])
    .on('error', console.log) // Если есть ошибки, выводим и продолжаем
    .pipe(concat('plugin.css')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
    .pipe(gulp.dest('./public/css/')) // записываем css
    .pipe(livereload(server)); // даем команду на перезагрузку css
});
// Собираем Stylus
gulp.task('css', function() {
    gulp.src(['./assets/css/main.css', './assets/css/media.css'])
    .on('error', console.log) // Если есть ошибки, выводим и продолжаем
    .pipe(concat('main.css')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
    .pipe(gulp.dest('./public/css/')) // записываем css
    .pipe(livereload(server)); // даем команду на перезагрузку css
});

gulp.task('fonts', function() {
    gulp.src('./assets/fonts/**/*')
    .on('error', console.log) // Если есть ошибки, выводим и продолжаем
    .pipe(gulp.dest('./public/fonts/')) // записываем css
    .pipe(livereload(server)); // даем команду на перезагрузку css
});



// Собираем html из Jade

gulp.task('jade', function() {
    gulp.src(['./assets/template/*.jade', '!./assets/template/_*.jade'])
    .pipe(jade({
        pretty: true
        }))  // Собираем Jade только в папке ./assets/template/ исключая файлы с _*
        .on('error', console.log) // Если есть ошибки, выводим и продолжаем
    .pipe(gulp.dest('./public/')) // Записываем собранные файлы
    .pipe(livereload(server)); // даем команду на перезагрузку страницы
});



// Собираем JS
gulp.task('js-plugin', function() {
    gulp.src(['./assets/js/**/*.js', '!./assets/js/main.js'])
        .pipe(concat('plugin.js')) // Собираем все JS, кроме тех которые находятся в ./assets/js/vendor/**
        .pipe(gulp.dest('./public/js'))
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
    });
// Собираем JS
gulp.task('js', function() {
    gulp.src(['./assets/js/main.js'])
    .pipe(gulp.dest('./public/js'))
        .pipe(livereload(server)); // даем команду на перезагрузку страницы
    });



// Копируем и минимизируем изображения

gulp.task('images', function() {
    gulp.src('./assets/img/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./public/img'))
});

gulp.task('images-watch', function() {
    gulp.src('./assets/img/**/*')
    .pipe(gulp.dest('./public/img'))
});


// Локальный сервер для разработки

gulp.task('http-server', function() {
    connect()
    .use(require('connect-livereload')())
    .use(serveStatic('./public'))
    .listen('9000');

    console.log('Server listening on http://localhost:9000');
});

// Запуск сервера разработки gulp watch
gulp.task('watch', function() {
    // Предварительная сборка проекта
    gulp.run('css');
    gulp.run('css-plugin');
    gulp.run('jade');
    gulp.run('images-watch');
    gulp.run('fonts');
    gulp.run('js');
    gulp.run('js-plugin');

    // Подключаем Livereload
    server.listen(35729, function(err) {
        if (err) return console.log(err);

        gulp.watch('assets/css/**/*.css', function() {
            gulp.run('css');
            gulp.run('css-plugin');
        });
        gulp.watch('assets/img/**/*', function() {
            gulp.run('images-watch');
        });
        gulp.watch('assets/js/**/*', function() {
            // gulp.run('js-plugin');
            gulp.run('js');
        });
    });
    gulp.run('http-server');
});

gulp.task('build', function() {
    // css
    gulp.src('./assets/stylus/screen.styl')
        .pipe(css({})) // собираем stylus
    .pipe(myth()) // добавляем префиксы - http://www.myth.io/
    .pipe(csso()) // минимизируем css
    .pipe(gulp.dest('./build/css/')) // записываем css

    // jade
    gulp.src(['./assets/template/*.jade', '!./assets/template/_*.jade'])
    .pipe(jade())
    .pipe(gulp.dest('./build/'))

    // // js
    // gulp.run('js-plugin');
    // gulp.run('js');
    // .pipe(uglify())
    // .pipe(gulp.dest('./build/js'));

    // image
    gulp.src('./assets/img/**/*')
    .pipe(imagemin())
    .pipe(gulp.dest('./build/img'))

});
