$(".drop-slider").slick({
	slidesToShow: 3,
	slidesToScroll: 1,
	arrows: true,
	dots: false,
	autoplay: true,
	responsive: [
  {
    breakpoint: 1200,
    settings: {
      slidesToShow: 2,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 767,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }
  ]
})

$(document).ready(function(){
  $(".burger button").on("click", function () {
    $(".burger button").toggleClass("active");
    $(".header__menu").slideToggle();
  })
});
$(".question-top p").on("click", function () {
  $(this).parent().toggleClass("active");
  $(this).parent().siblings(".ansver").slideToggle();
  $(this).parents(".question-wrap").siblings().children(".ansver").slideUp();
  $(this).parents(".question-wrap").siblings().children(".question-top").removeClass("active");;
})
$(document).ready(function(){
  $(".play-btn a").on("click", function(e) {
    e.preventDefault();
  });
  $(".play-btn").on("click", function gameInit() {
    $(".case-open__ico").hide();
    $(".carousel").show();
    $(".pricel").show();
    // Слайдер
    $('.owl-carousel').owlCarousel({
      loop : false,
      item : 3,
      mouseDrag : false,
      touchDrag : false,
      pullDrag : false,
      center:true,
      URLhashListener : true,
      startPosition: 'URLHash'
    });
    // Генератор числа
    function randomInteger(min, max) {
      var rand = min - 0.5 + Math.random() * (max - min + 1)
      rand = Math.round(rand);
      return rand;
    }
    var random_item = randomInteger(0, 32);
    // Передаем сгенерированое число в атрибут ссылки(к чему будет крутиться)
    $('.play-btn').attr('href', '#' + random_item);
    // Шторки
    $('.doors_left').delay(2000).animate({left: '0'}, 400).delay(2000).animate({left: '-50vw'}, 400);
    $('.doors_right').delay(2000).animate({right: '0'}, 400).delay(2000).animate({right: '-50vw'}, 400);
    $('.doors .weapon-item').delay(2200).fadeIn().delay(1800).fadeOut();
    // Вывод выиграного оружия
    $('.owl-item .item_car').each(function(i){
      var data_item = $(this).attr('data-hash');
      if (data_item == random_item) {
        console.log(this);
        var html_item = $(this).html();
        $('.doors .weapon-item').html(html_item);
      }
    });
  });
})

$(document).ready(function(){
  $(".case-item--1").on("click", function gameInit() {
    $(".case-open__ico--1").show();
    $(".case-open__ico--2").hide();
    $(".case1").show();
    $(".case2").hide();
    $(".carousel").hide();
  })
})
$(document).ready(function(){
	$(".case-item--2").on("click", function gameInit() {
    $(".case-open__ico--2").show();
    $(".case-open__ico--1").hide();
    $(".case2").show();
    $(".case1").hide();
    $(".carousel").hide();
  })
})

function tabInit(){ 
  $('.tabs__content').not('.tabs__content_active').hide(); 
  $('.tabs-list__item button').click(function(){ 
    $('.tabs-list__item').not($(this).parent()).removeClass('tabs-list__item_active'); 
    $(this).parent().addClass('tabs-list__item_active'); 
    $('.tabs__content').not('#'+$(this).attr('data-content')).removeClass('tabs__content_active').hide(); 
    $('#'+$(this).attr('data-content')).addClass('tabs__content_active').fadeIn(); 
  }); 
}; 
tabInit();