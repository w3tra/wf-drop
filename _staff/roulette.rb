def game_one
  rand_number = rand(0..10_000)
  case rand_number
  when 0..1000      then 30
  when 1001..5099   then 60
  when 5417..10_000 then 90
  when 5100..5250   then 160
  when 5251..5300   then 200
  when 5301..5400   then 300
  when 5401..5410   then 500
  when 5411..5412   then 750
  when 5413..5414   then 1000
  when 5415..5416   then 2000
  end
end

def game_two
  rand_number = rand(0..10_000)
  case rand_number
  when 1..1000     then 40
  when 1001..5000  then 80
  when 5421..10_000 then 160
  when 5001..5200  then 320
  when 5201..5300  then 400
  when 5301..5400  then 500
  when 5401..5410  then 750
  when 5411..5412  then 1000
  when 5413..5414  then 1500
  when 5415..5416  then 2500
  when 5416..5417  then 5000
  when 5418..5419  then 7500
  when 5420        then 10_000
  end
end

p game_one
p game_two